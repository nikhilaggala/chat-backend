const express = require('express');
const socketio = require('socket.io');
const cors = require('cors');
const http = require('http');
const router = require('./router');

const { addUser, removeUser, getUser, getUsersInRoom } = require('./users');

const app = express();
const server = http.createServer(app);
const io = socketio(server);
var port = process.env.PORT || 5000;

app.use(cors());

io.on('connection', (socket) => {
  socket.on('join', ({ name, room, avatar }, callBack) => {
    const { error, user } = addUser({ id: socket.id, name, room, avatar });

    if (error) {
      return callBack(error);
    }

    // Welcome msg to the particular user who had joined
    socket.emit('message', { user: 'admin', text: `Hey ${user.name}!, Welcome to the room - ${user.room}`});

    // Telling all others in the particular room tha he had joined
    socket.broadcast.to(user.room).emit('message', { user: 'admin', text: `${user.name} had joined!` });

    socket.join(user.room);

    io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });


    // Just randomly calling
    callBack();
  });

  socket.on('sendMessage', (message, callBack) => {
    const user = getUser(socket.id);

    io.to(user.room).emit('message', { user: user.name, text: message, avatar: user.avatar });
    io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });

    callBack();

  });

  socket.on('disconnect', () => {
    console.log('Connection disconnected :)');
    const user = removeUser(socket.id);

    if (user) {
      io.to(user.room).emit('message', { user: 'admin', text: `${user.name} is left!` })
    }
  });
});

app.use(router);


server.listen(port, () => {
  console.log(`listening on ${port}`);
});
